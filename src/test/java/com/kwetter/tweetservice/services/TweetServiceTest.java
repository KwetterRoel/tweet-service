package com.kwetter.tweetservice.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kwetter.tweetservice.domain.FollowImport;
import com.kwetter.tweetservice.domain.entities.Tweet;
import com.kwetter.tweetservice.domain.entities.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class TweetServiceTest {

    @Autowired
    TweetService tweetService;

    @Autowired
    UserService userService;

    @Autowired
    FollowService followService;

    @Test
    void getTweetsWithNullAcc() {
        assertThat(tweetService.getAllTweets(null, 0, 0).getStatusCode())
                .isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    void getAllTweets() {
        userService.saveUser(new User(1L, "TU@test.nl", "TU", null, null));
        assertThat(tweetService.getAllTweets("TU@test.nl", 0, 10).getStatusCode())
                .isEqualTo(HttpStatus.OK);
    }

    @Test
    void addTweet() {
        String userEmail = "TU2@test.nl";
        userService.saveUser(new User(2L, userEmail, "TU2", null, null));
        int size = Objects.requireNonNull(tweetService.getAllTweets(userEmail, 0, 10).getBody()).getContent().size();
        tweetService.addNewTweet(userEmail, "test tweet");

        assertThat(Objects.requireNonNull(tweetService.getAllTweets(userEmail, 0, 11).getBody()).getContent().size())
                .isNotEqualTo(size);
    }

    @Test
    void canGetAllTweetsByUser() {
        userService.saveUser(new User(3L, "TU3@test.nl", "TU3", null, null));
        assertThat(tweetService.getTweetsAmountByUser(3L).getStatusCode())
                .isEqualTo(HttpStatus.OK);
    }

    @Test
    void addTweetAndGetByUser() {
        String userEmail = "TU4@test.nl";
        userService.saveUser(new User(4L, userEmail, "TU4", null, null));
        int size = Objects.requireNonNull(tweetService.getAllTweetsByUser(userEmail, 4L, 0, 10).getBody()).getContent().size();
        tweetService.addNewTweet(userEmail, "test tweet");

        assertThat(Objects.requireNonNull(tweetService.getAllTweetsByUser(userEmail, 4L, 0, 10).getBody()).getContent().size())
                .isNotEqualTo(size);
    }

    @Test
    void getTweetsAmountByUser() {
        String userEmail = "TU5@test.nl";
        userService.saveUser(new User(5L, userEmail, "TU5", null, null));
        Long size = tweetService.getTweetsAmountByUser(5L).getBody();
        tweetService.addNewTweet(userEmail, "test tweet");

        assertThat(tweetService.getTweetsAmountByUser(5L).getBody())
                .isNotEqualTo(size);
    }

    @Test
    void likeTweet() {
        String userEmail = "TU5@test.nl";
        userService.saveUser(new User(5L, userEmail, "TU5", null, null));
        tweetService.addNewTweet(userEmail, "test tweet");

        Tweet tweet = Objects.requireNonNull(tweetService.getAllTweets(userEmail, 0, 10).getBody()).getContent().get(0);


        assertThat(tweetService.likeTweet(userEmail, tweet.getId()).getStatusCode())
                .isEqualTo(HttpStatus.OK);
    }

    @Test
    void unLikeTweet() {
        String userEmail = "TU6@test.nl";
        userService.saveUser(new User(6L, userEmail, "TU6", null, null));
        tweetService.addNewTweet(userEmail, "test tweet");

        Tweet tweet = Objects.requireNonNull(tweetService.getAllTweets(userEmail, 0, 10).getBody()).getContent().get(0);

        assertThat(tweetService.likeTweet(userEmail, tweet.getId()).getStatusCode())
                .isEqualTo(HttpStatus.OK);

        assertThat(tweetService.unLikeTweet(userEmail, tweet.getId()).getStatusCode())
                .isEqualTo(HttpStatus.OK);
    }

    @Test
    void deleteTweet() {
        String userEmail = "TU7@test.nl";
        userService.saveUser(new User(7L, userEmail, "TU7", null, null));
        int size = Objects.requireNonNull(tweetService.getAllTweets(userEmail, 0, 10).getBody()).getContent().size();
        tweetService.addNewTweet(userEmail, "test tweet");

        assertThat(Objects.requireNonNull(tweetService.getAllTweets(userEmail, 0, 10).getBody()).getContent().size())
                .isNotEqualTo(size);

        size = Objects.requireNonNull(tweetService.getAllTweets(userEmail, 0, 10).getBody()).getContent().size();

        Tweet tweet = Objects.requireNonNull(tweetService.getAllTweets(userEmail, 0, 10).getBody()).getContent().get(0);

        tweetService.deleteTweet(userEmail, tweet.getId());

        assertThat(Objects.requireNonNull(tweetService.getAllTweets(userEmail, 0, 10).getBody()).getContent().size())
                .isEqualTo(size - 1);
    }

    @Test
    void getTimelineNoFollowers(){
        String userEmail = "TU8@test.nl";
        userService.saveUser(new User(8L, userEmail, "TU8", null, null));

        tweetService.addNewTweet(userEmail, "test tweet");

        int size = Objects.requireNonNull(tweetService.getTimeline(userEmail, 0, 10).getBody()).getContent().size();

        assertThat(size)
                .isZero();
    }

    @Test
    void getTimeline() {
        String userEmail = "TU9@test.nl";
        String followEmail = "TU10@test.nl";
        userService.saveUser(new User(9L, userEmail, "TU9", null, null));
        userService.saveUser(new User(10L, followEmail, "TU10", null, null));

        tweetService.addNewTweet(followEmail, "test tweet");

        int size = Objects.requireNonNull(tweetService.getTimeline(userEmail, 0, 10).getBody()).getContent().size();
        assertThat(size)
                .isZero();


        followService.followUser(new FollowImport(9L, 10L));


        size = Objects.requireNonNull(tweetService.getTimeline(userEmail, 0, 10).getBody()).getContent().size();
        assertThat(size)
                .isNotZero();
    }
}
