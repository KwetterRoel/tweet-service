package com.kwetter.tweetservice.repositories;

import com.kwetter.tweetservice.domain.entities.Tweet;
import com.kwetter.tweetservice.domain.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TweetRepository extends CrudRepository<Tweet, Long> {
    Page<Tweet> findAllByOrderByCreatedDesc(Pageable pageable);
    Page<Tweet> findAllByUserIdOrderByCreatedDesc(Long id, Pageable pageable);
    Long countAllByUserId(Long id);

    @Query("Select t from Tweet t WHERE t.user IN :following ORDER BY t.created DESC")
    Page<Tweet> generateTimeline(@Param("following")List<User> following, Pageable pageable);

}
