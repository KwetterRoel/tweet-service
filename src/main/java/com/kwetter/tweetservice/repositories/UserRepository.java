package com.kwetter.tweetservice.repositories;


import com.kwetter.tweetservice.domain.entities.User;
import org.springframework.data.repository.CrudRepository;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByEmail(String email);

}
