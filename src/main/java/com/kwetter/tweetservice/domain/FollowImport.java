package com.kwetter.tweetservice.domain;

public class FollowImport {

    private Long account_id;
    private Long follow_id;

    public FollowImport(){}
    public FollowImport(Long account_id, Long follow_id){
        this.account_id = account_id;
        this.follow_id = follow_id;
    }

    public Long getAccount_id() {
        return account_id;
    }

    public void setAccount_id(Long account_id) {
        this.account_id = account_id;
    }

    public Long getFollow_id() {
        return follow_id;
    }

    public void setFollow_id(Long follow_id) {
        this.follow_id = follow_id;
    }
}
