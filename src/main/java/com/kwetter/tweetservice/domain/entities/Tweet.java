package com.kwetter.tweetservice.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

import java.util.Date;


@Entity
public class Tweet {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private User user;

    private String message;
    private Date created;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL)
    private List<Likes> likes;

    @Transient
    private Boolean liked;
    @Transient
    private int totalLikes;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<Likes> getLikes() {
        return likes;
    }

    public void setLikes(List<Likes> likes) {
        this.likes = likes;
    }

    public Boolean getLiked() {
        return liked;
    }

    public void addLike(Likes like) {
        this.getLikes().add(like);
    }

    public void removeLike(Likes like) {
        this.getLikes().remove(like);
    }

    public void setLiked(Boolean liked) {
        this.liked = liked;
    }

    public int getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(int totalLikes) {
        this.totalLikes = totalLikes;
    }
}
