package com.kwetter.tweetservice.domain.entities;

import javax.persistence.*;

@Entity
public class Likes {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private User user;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
