package com.kwetter.tweetservice.configuration;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class Beanstock {

    @Bean
    public Queue newUserQueue() {
        return new Queue("CustomerService_NewUser", true);
    }

    @Bean
    public Queue updateUserQueue() {
        return new Queue("CustomerService_UpdateUser", true);
    }

    @Bean
    public Queue followUserQueue() {
        return new Queue("FollowService_FollowUser", true);
    }
}
