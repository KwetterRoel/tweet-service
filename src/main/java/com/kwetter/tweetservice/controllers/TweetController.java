package com.kwetter.tweetservice.controllers;

import com.kwetter.tweetservice.domain.entities.Tweet;
import com.kwetter.tweetservice.services.TweetService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
public class TweetController {

    private TweetService tweetService;

    public TweetController(TweetService tweetService) {
        this.tweetService = tweetService;
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/")
    public ResponseEntity<Page<Tweet>> getAllTweets(@ApiIgnore Authentication authentication, @RequestParam(value = "page", defaultValue = "0") int page, @RequestParam(value = "size", defaultValue = "10") int size) {
        return tweetService.getAllTweets(authentication.getName(), page, size);
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/timeline")
    public ResponseEntity<Page<Tweet>> getTimeline(@ApiIgnore Authentication authentication, @RequestParam(value = "page", defaultValue = "0") int page, @RequestParam(value = "size", defaultValue = "10") int size) {
        return tweetService.getTimeline(authentication.getName(), page, size);
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/byUser/{id}")
    public ResponseEntity<Page<Tweet>> getAllTweetsByUser(@ApiIgnore Authentication authentication, @PathVariable Long id, @RequestParam(value = "page", defaultValue = "0") int page, @RequestParam(value = "size", defaultValue = "10") int size) {
        return tweetService.getAllTweetsByUser(authentication.getName(), id, page, size);
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/byUser/{id}/amount")
    public ResponseEntity<Long> getTweetsAmountByUser(@PathVariable Long id) {
        return tweetService.getTweetsAmountByUser(id);
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/")
    public ResponseEntity<String> addNewTweet(@ApiIgnore Authentication authentication, @RequestBody String tweetMessage) {
        return tweetService.addNewTweet(authentication.getName(), tweetMessage);
    }

    @PreAuthorize("isAuthenticated()")
    @PatchMapping("/{id}/like")
    public ResponseEntity<String> likeTweet(@ApiIgnore Authentication authentication, @PathVariable Long id) {
        return tweetService.likeTweet(authentication.getName(), id);
    }

    @PreAuthorize("isAuthenticated()")
    @PatchMapping("/{id}/unLike")
    public ResponseEntity<String> unLikeTweet(@ApiIgnore Authentication authentication, @PathVariable Long id) {
        return tweetService.unLikeTweet(authentication.getName(), id);
    }

    @PreAuthorize("isAuthenticated()")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteTweet(@ApiIgnore Authentication authentication, @PathVariable Long id) {
        return tweetService.deleteTweet(authentication.getName(), id);
    }
}

