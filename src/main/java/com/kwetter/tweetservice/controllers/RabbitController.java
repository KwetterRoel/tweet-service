package com.kwetter.tweetservice.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kwetter.tweetservice.domain.FollowImport;
import com.kwetter.tweetservice.domain.entities.User;
import com.kwetter.tweetservice.services.FollowService;
import com.kwetter.tweetservice.services.UserService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class RabbitController {
    private UserService userService;
    private FollowService followService;

    public RabbitController(UserService userService, FollowService followService) {
        this.userService = userService;
        this.followService = followService;
    }

    @RabbitListener(queues = {"CustomerService_NewUser"})
    public void registerNewUser(String message) throws JsonProcessingException {
        User user = new ObjectMapper().readValue(message, User.class);
        userService.saveUser(user);
    }

    @RabbitListener(queues = {"CustomerService_UpdateUser"})
    public void updateUser(String message) throws JsonProcessingException {
        User user = new ObjectMapper().readValue(message, User.class);
        userService.saveUser(user);
    }

    @RabbitListener(queues = {"FollowService_FollowUser"})
    public void followUser(String message) throws JsonProcessingException {
        //read message
        FollowImport follow = new ObjectMapper().readValue(message, FollowImport.class);
        followService.followUser(follow);
    }

}
