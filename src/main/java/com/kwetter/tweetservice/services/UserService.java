package com.kwetter.tweetservice.services;

import com.kwetter.tweetservice.domain.entities.User;
import com.kwetter.tweetservice.repositories.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public void saveUser(User user) {
        userRepository.save(user);
    }
}
