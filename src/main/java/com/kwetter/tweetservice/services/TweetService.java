package com.kwetter.tweetservice.services;

import com.kwetter.tweetservice.domain.entities.Likes;
import com.kwetter.tweetservice.domain.entities.Tweet;
import com.kwetter.tweetservice.domain.entities.User;
import com.kwetter.tweetservice.repositories.TweetRepository;
import com.kwetter.tweetservice.repositories.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class TweetService {
    private TweetRepository tweetRepository;
    private UserRepository userRepository;

    public TweetService(TweetRepository tweetRepository, UserRepository userRepository) {
        this.tweetRepository = tweetRepository;
        this.userRepository = userRepository;
    }

    public ResponseEntity<Page<Tweet>> getAllTweets(String userEmail, int page, int size) {
        Optional<User> optionalAccount = userRepository.findByEmail(userEmail);

        // check if account exists
        if (optionalAccount.isPresent()) {
            User thisUser = optionalAccount.get();
            Page<Tweet> tweets = tweetRepository.findAllByOrderByCreatedDesc(PageRequest.of(page, size));
            return getPageResponseEntity(thisUser, tweets);
        }

        //user not exist
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
    }

    public ResponseEntity<Page<Tweet>> getTimeline(String userEmail, int page, int size) {
        Optional<User> optionalAccount = userRepository.findByEmail(userEmail);

        // check if account exists
        if (optionalAccount.isPresent()) {
            User thisUser = optionalAccount.get();
            Page<Tweet> tweets = tweetRepository.generateTimeline(thisUser.getFollowing(), PageRequest.of(page, size));
            return getPageResponseEntity(thisUser, tweets);
        }

        //user not exist
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
    }

    public ResponseEntity<Page<Tweet>> getAllTweetsByUser(String userEmail, Long id, int page, int size) {
        Optional<User> optionalAccount = userRepository.findByEmail(userEmail);

        // check if account exists
        if (optionalAccount.isPresent()) {
            User thisUser = optionalAccount.get();
            Page<Tweet> tweets = tweetRepository.findAllByUserIdOrderByCreatedDesc(id, PageRequest.of(page, size));
            return getPageResponseEntity(thisUser, tweets);
        }

        //user not exist
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
    }

    public ResponseEntity<Long> getTweetsAmountByUser(Long id) {
        return ResponseEntity.ok().body(tweetRepository.countAllByUserId(id));
    }

    public ResponseEntity<String> addNewTweet(String userEmail, String tweetMessage) {
        Optional<User> optionalAccount = userRepository.findByEmail(userEmail);

        // check if account exists
        if (optionalAccount.isPresent()) {
            Tweet tweet = new Tweet();
            tweet.setUser(optionalAccount.get());
            tweet.setMessage(tweetMessage);
            tweet.setCreated(new Date());
            tweetRepository.save(tweet);
            return ResponseEntity.ok().body("200");
        }

        //user not exist
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
    }

    public ResponseEntity<String> likeTweet(String userEmail, Long id) {
        Optional<User> optionalAccount = userRepository.findByEmail(userEmail);

        // check if account exists
        if (optionalAccount.isPresent()) {
            Optional<Tweet> optionalTweet = tweetRepository.findById(id);
            //if tweet exist
            if (optionalTweet.isPresent()) {
                Tweet tweet = optionalTweet.get();
                Likes like = new Likes();
                like.setUser(optionalAccount.get());
                tweet.addLike(like);
                tweetRepository.save(tweet);
                //user not exist
                return ResponseEntity.ok().body("200");
            }
            //tweet not exist
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

        //user not exist
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
    }

    public ResponseEntity<String> unLikeTweet(String userEmail, Long id) {
        Optional<User> optionalAccount = userRepository.findByEmail(userEmail);

        // check if account exists
        if (optionalAccount.isPresent()) {
            User thisUser = optionalAccount.get();
            Optional<Tweet> optionalTweet = tweetRepository.findById(id);
            //if tweet exist
            if (optionalTweet.isPresent()) {
                Tweet tweet = optionalTweet.get();
                Optional<Likes> optionalLike = tweet.getLikes().stream().filter(l -> l.getUser().getId().equals(thisUser.getId())).findFirst();
                if (optionalLike.isPresent()) {
                    tweet.removeLike(optionalLike.get());
                    tweetRepository.save(tweet);
                    return ResponseEntity.ok().body("200");
                }
                //like not exist
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }
            //tweet not exist
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        //user not exist
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
    }

    public ResponseEntity<String> deleteTweet(String userEmail, Long id) {
        Optional<User> optionalAccount = userRepository.findByEmail(userEmail);
        Optional<Tweet> optionalTweet = tweetRepository.findById(id);

        // check if account exists
        if (optionalAccount.isPresent()) {
            User thisUser = optionalAccount.get();
            if (optionalTweet.isPresent()) {
                if (optionalTweet.get().getUser().getId().equals(thisUser.getId())) {
                    tweetRepository.deleteById(id);
                    return ResponseEntity.ok().body("200");
                }
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

        //user not exist
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
    }

    private ResponseEntity<Page<Tweet>> getPageResponseEntity(User thisUser, Page<Tweet> tweets) {
        tweets.getContent().forEach(tweet -> {
            try {
                tweet.setTotalLikes(tweet.getLikes().size());
                tweet.setLiked(tweet.getLikes().stream().map(Likes::getUser).anyMatch(thisUser::equals));
            } catch (Exception e) {
            }
        });
        return ResponseEntity.ok().body(tweets);
    }
}
