package com.kwetter.tweetservice.services;

import com.kwetter.tweetservice.domain.FollowImport;
import com.kwetter.tweetservice.domain.entities.User;
import com.kwetter.tweetservice.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FollowService {

    private UserService userService;
    private UserRepository userRepository;

    public FollowService(UserService userService, UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    public void followUser(FollowImport follow) {
        //get users
        Optional<User> optionalAccount = userRepository.findById(follow.getAccount_id());
        Optional<User> optionalToFollow = userRepository.findById(follow.getFollow_id());

        //if account
        if (optionalAccount.isPresent()) {
            //get account
            User thisUser = optionalAccount.get();
            //if follow account
            if (optionalToFollow.isPresent()) {
                //get account
                User toFollow = optionalToFollow.get();
                //if is following
                if (userExists(thisUser.getFollowing(), toFollow)) {
                    //remove following
                    thisUser.setFollowing(removeUser(thisUser.getFollowing(), toFollow));
                } else {
                    //add following
                    thisUser.getFollowing().add(toFollow);
                }
                //update user
                userService.saveUser(thisUser);
            }
        }
    }

    private boolean userExists(List<User> users, User user) {
        for (User u: users) {
            if (u.getId().equals(user.getId())){
                return true;
            }
        }
        return false;
    }

    private List<User> removeUser(List<User> users, User user) {
        for (User u: users) {
            if (u.getId().equals(user.getId())){
                users.remove(u);
                return users;
            }
        }
        return users;
    }

}
